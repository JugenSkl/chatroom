﻿using Chat.Base.Enums;
using Chat.Base.Interfaces;

namespace Сhat.Client.Interfaces
{
    public interface ILocationServiceAlert<T>: ILocationService<T>
    {
        #region Methods
        public void TransmitData(TypeSM type, object data);
        public void SetListener(T obj, TypeSM type);
        #endregion
    }
}
