﻿using Chat.Base.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Сhat.Client.Enums;
using Сhat.Client.Services;
using Сhat.Client.View;
using Сhat.Client.ViewModel;

namespace Сhat.Client
{
    public partial class MainWindow : Window
    {
        #region Constructors
        public MainWindow()
        {
            InitializeComponent();
            _sourceServices = new SourceServices();
            DataContext = new MainWindowViewModel(_sourceServices);
            AuthorisationViewModel viewModel = new AuthorisationViewModel(_sourceServices);
            _sourceServices.GetLSA().SetRegion(new AuthorisationView { DataContext = viewModel }, Regions.Central);
            Application.Current.Exit += ApplicationExit;
        }
        #endregion

        #region Fields
        private SourceServices _sourceServices;
        #endregion

        #region Event Handling
        private void ApplicationExit(object sender, ExitEventArgs e)
        {
            var client = _sourceServices.GetClient();
            if (client != null)
                client.Disconnect(TypeDiconect.ManualDisconnect);
        }
        #endregion
    }
}
