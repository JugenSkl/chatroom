﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Сhat.Base;
using Сhat.Base.Classes;
using Prism.Commands;
using Сhat.Client.Interfaces;
using Chat.Base.Services;
using Chat.Base.Interfaces;
using Сhat.Client.Services;
using Chat.Base.Enums;
using Сhat.Client.View;
using Сhat.Client.Enums;

namespace Сhat.Client.ViewModel
{
    public class ListUsersViewModel: ViewModelBase, ISubscriber
    {
        #region Constructors
        public ListUsersViewModel(SourceServices sourceServices, User user, List<User> users)
        {
            _user = user;
            Users = new ObservableCollection<User>(users);
            Users.Remove(_user);
            _sourceServices = sourceServices;
            _locationService = _sourceServices.GetLSA();
            _locationService.SetListener(this, TypeSM.Authorisation);
            _locationService.SetListener(this, TypeSM.ChangingUserState);
            OpenChatCommand = new DelegateCommand<User>(DoOpenChat);
            LogOutCommand = new DelegateCommand(DoLogOut);
            _synchronizationContext = SynchronizationContext.Current;
        }
        #endregion

        #region Fields
        private SynchronizationContext _synchronizationContext;
        private ILocationServiceAlert<ISubscriber> _locationService;
        private SourceServices _sourceServices;
        #endregion

        #region Properties
        private User _user;
        public User User
        {
            get { return _user; }
            set { SetProperty(ref _user, value); }
        }
        private ObservableCollection<User> _users;
        public ObservableCollection<User> Users
        {
            get { return _users; }
            set { SetProperty(ref _users, value); }
        }
        #endregion

        #region Commands
        public DelegateCommand<User> OpenChatCommand { get; private set; }
        private void DoOpenChat(User user)
        {
            _synchronizationContext.Send(_ =>
            {
                var viewModel = new ChatViewModel(user, _sourceServices);
                _locationService.SetRegion(new ChatView { DataContext = viewModel }, Regions.Central);
            }, null);
        }

        public DelegateCommand LogOutCommand { get; private set; }
        private void DoLogOut()
        {
            var client = _sourceServices.GetClient();
            client.Disconnect(TypeDiconect.ManualDisconnect);
        }
        #endregion

        #region Implementing ISubscriber
        public void Update(object data)
        {
            if (data is List<User>)
            {
                List<User> users = data as List<User>;
                foreach (var user in users)
                {
                    if (_user.Name != user.Name && !Users.Contains(user))
                    {
                        _synchronizationContext.Send(_ =>
                        {
                            Users.Add(user);
                        }, null);
                    }
                }
            }
            else if (data is User)
            {
                User user = data as User;

                var _us = Users.FirstOrDefault(_ => _.Id == user.Id);
                if (_us != null)
                {
                    _synchronizationContext.Send(_ =>
                    {
                        _us.IsOnline = user.IsOnline;
                    }, null);
                }
                else
                {
                    _synchronizationContext.Send(_ =>
                    {
                        Users.Add(user);
                    }, null);
                }
            }
        }
        #endregion
    }
}
