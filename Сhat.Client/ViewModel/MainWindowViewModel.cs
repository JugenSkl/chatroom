﻿using Chat.Base.Enums;
using Chat.Base.Interfaces;
using System.Windows;
using Сhat.Base;
using Сhat.Client.Interfaces;
using Сhat.Client.Services;
using Сhat.Client.View;

namespace Сhat.Client.ViewModel
{
    public class MainWindowViewModel: ViewModelBase
    {
        #region Constructors
        public MainWindowViewModel(SourceServices sourceServices)
        {
            _sourceServices = sourceServices;
            _locationService = _sourceServices.GetLSA();
        }
        #endregion

        #region Fields
        private SourceServices _sourceServices;
        #endregion

        #region Commands
        private ILocationServiceAlert<ISubscriber> _locationService;
        public ILocationServiceAlert<ISubscriber> LocationS
        {
            get { return _locationService; }
            init { SetProperty(ref _locationService, value); }
        }
        #endregion
    }
}
