﻿using Chat.Base.Enums;
using Сhat.Base;
using Сhat.Base.Classes;
using Сhat.Client.Interfaces;
using Сhat.Client.Services;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using Prism.Commands;
using Chat.Base.Interfaces;
using System.Windows;

namespace Сhat.Client.ViewModel
{
    public class ChatViewModel: ViewModelBase, ISubscriber
    {
        #region Constructors
        public ChatViewModel(User user, SourceServices sourceServices)
        {
            User = user;

            _sourceServices = sourceServices;
            _clientService = _sourceServices.GetClient();
            _clientService.GetMessages(User);
            _locationService = _sourceServices.GetLSA();
            _locationService.SetListener(this, TypeSM.Message);

            Messages = new ObservableCollection<Message>();
            SendCommand = new DelegateCommand(DoSend);
            _synchronizationContext = SynchronizationContext.Current;
        }
        #endregion

        #region Fields
        private ClientService _clientService;
        private SourceServices _sourceServices;
        private ILocationServiceAlert<ISubscriber> _locationService;
        private SynchronizationContext _synchronizationContext;
        #endregion

        #region Properties
        private ObservableCollection<Message> _messages;
        public ObservableCollection<Message> Messages
        {
            get { return _messages; }
            set { SetProperty(ref _messages, value); }
        }

        private User _user;
        public User User
        {
            get { return _user; }
            set { SetProperty(ref _user, value); }
        }

        private string _textMessage;
        public string TextMessage
        {
            get { return _textMessage; }
            set { SetProperty(ref _textMessage, value); }
        }
        #endregion

        #region Commands
        public DelegateCommand SendCommand { get; private set; }
        private void DoSend()
        {
            _clientService.SendMessages(User, TextMessage);
            TextMessage = string.Empty;
        }
        #endregion

        #region Implementing ISubscriber
        public void Update(object data)
        {
            if (data is List<Message>)
            {
                var messages = data as List<Message>;
                foreach (var message in messages)
                {
                    if (message.Recipient.Id == User.Id || message.Sender.Id == User.Id)
                    {
                        if (User.Id == message.Sender.Id)
                            message.Orientation = HorizontalAlignment.Right;
                        else
                            message.Orientation = HorizontalAlignment.Left;

                        _synchronizationContext.Send(_ =>
                        {
                            Messages.Add(message);
                        }, null);
                    }
                }

            }
        }
        #endregion
    }
}
