﻿using Сhat.Base;
using Chat.Base.Enums;
using Сhat.Client.Resources;
using Сhat.Client.Services;
using Сhat.Client.View;
using Сhat.Base.Classes;
using Сhat.Client.Interfaces;
using Prism.Commands;
using System.Threading;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Linq;
using Chat.Base.View;
using Chat.Base.Interfaces;
using System.Threading.Tasks;
using Сhat.Client.Enums;
using System;

namespace Сhat.Client.ViewModel
{
    public class AuthorisationViewModel: ViewModelBase, ISubscriber
    {
        #region Constructors
        public AuthorisationViewModel(SourceServices sourceServices)
        {
            _sourceServices = sourceServices;
            _locationService = _sourceServices.GetLSA();
            _locationService.SetListener(this, TypeSM.Authorisation);
            _locationService.SetListener(this, TypeSM.Error);
            AuthorisationСommand = new DelegateCommand(DoAuthorisation);
            CreateAccountCommand = new DelegateCommand(DoCreateAccount);
            Host = "127.0.0.1";
            Port = "8888";
        }
        #endregion

        #region Fields
        private SourceServices _sourceServices;
        private SynchronizationContext _synchronizationContext;
        private ILocationServiceAlert<ISubscriber> _locationService;
        #endregion

        #region Properties
        private string _userName;
        public string UserName
        {
            get { return _userName; }
            set { SetProperty(ref _userName, value); }
        }

        private string _host;
        public string Host
        {
            get { return _host; }
            set { SetProperty(ref _host, value); }
        }

        private string _port;
        public string Port
        {
            get { return _port; }
            set { SetProperty(ref _port, value); }
        }

        private string _erroString;
        public string ErroString
        {
            get { return _erroString; }
            set { SetProperty(ref _erroString, value); }
        }
        #endregion

        #region Commands
        public DelegateCommand AuthorisationСommand { get; private set; }
        private void DoAuthorisation()
        {
            if (!string.IsNullOrEmpty(UserName))
            {
                _synchronizationContext = SynchronizationContext.Current;
                var view = new LockScreenView(StringResources.Message_Boot);
                _locationService.SetRegion(view, Regions.Lockout);
                try
                {
                    UriHostNameType type = Uri.CheckHostName(Host);
                    if (type == UriHostNameType.Dns || type == UriHostNameType.Basic || type == UriHostNameType.Unknown)
                    {
                        ErroString = StringResources.Message_Wrong_Host_Set;
                        _locationService.SetRegion(null, Regions.Lockout);
                        return;
                    }
                    int port = int.Parse(Port);
                    
                    if (port < 0 || port > 65535)
                    {
                        _locationService.SetRegion(null, Regions.Lockout);
                        ErroString = StringResources.Message_Invalid_Port_Value;
                        return;
                    }
                    Task.Run(() => {
                        try
                        {
                            _sourceServices.InitializeClient(Host, port, _synchronizationContext);
                            var clientService = _sourceServices.GetClient();
                            clientService.ClientAuthorisation(UserName);
                        }
                        catch (SocketException)
                        {
                            ErroString = string.Format(StringResources.Message_Server_Connection_Error, Host, Port);
                            _locationService.SetRegion(null, Regions.Lockout);
                        }
                    });
                }
                catch (FormatException)
                {
                    ErroString = StringResources.Message_Wrong_Port_Set;
                    _locationService.SetRegion(null, Regions.Lockout);
                }  
            }
        }

        public DelegateCommand CreateAccountCommand { get; init; }
        private void DoCreateAccount()
        {
            _locationService.SetRegion(null, Regions.Central);
            var viewModel = new CreateAccountViewModel(_sourceServices);
            _locationService.SetRegion(new CreateAccountView { DataContext = viewModel }, Regions.Central);

        }
        #endregion

        #region Implementing ISubscriber
        public void Update(object data)
        {
            if (data is List<User>)
            {
                var users = data as List<User>;
                _locationService.SetRegion(null, Regions.Central);
                Task.Run(() =>
                {
                    Thread.Sleep(500);
                    _locationService.SetRegion(null, Regions.Lockout);
                });
                _synchronizationContext.Send(_ =>
                {
                    var viewModel = new ListUsersViewModel(_sourceServices, users.First(_ => _.Name == UserName), users);
                    _locationService.SetRegion(new ListUsersView(viewModel), Regions.Left);
                }, null);
            }
            else
            {
                ErroString = data.ToString();
                _sourceServices.GetClient().Disconnect(TypeDiconect.AuthorizationError);
                _locationService.SetRegion(null, Regions.Lockout);
            }
        }
        #endregion
    }
}
