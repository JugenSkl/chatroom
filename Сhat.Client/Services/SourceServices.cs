﻿using Chat.Base.Interfaces;
using System.Threading;

namespace Сhat.Client.Services
{
    public class SourceServices
    {
        #region Constructors
        public SourceServices()
        {
            _instanceLSA = new LocationServiceAlert<ISubscriber>();
        }
        #endregion

        #region Fields
        private LocationServiceAlert<ISubscriber> _instanceLSA;
        private ClientService _instanceClient;
        #endregion

        #region Methods
        public LocationServiceAlert<ISubscriber> GetLSA()
        {
            return _instanceLSA;
        }

        public ClientService GetClient()
        {
            return _instanceClient;
        }

        public void InitializeClient(string host, int port, SynchronizationContext sC)
        {
            _instanceClient = new ClientService(this, sC, host, port);
        }
        #endregion
    }
}