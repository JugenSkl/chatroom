﻿using Chat.Base.Enums;
using Chat.Base.Interfaces;
using Chat.Base.Services;
using Сhat.Client.Interfaces;
using Сhat.Client.Resources;

namespace Сhat.Client.Services
{
    public class LocationServiceAlert<T> : LocationService<T>, ILocationServiceAlert<T>
    {
        #region Constructors
        public LocationServiceAlert() { }
        #endregion

        #region Fields
        private T _registrationExpects;
        private T _authorisationExpects;
        private T _messageExpects;
        private T _changingUserStateExpects;
        private T _errorExpects;
        #endregion

        #region Methods
        public void SetListener(T obj, TypeSM type)
        {
            switch (type)
            {
                case TypeSM.Registration:
                    _registrationExpects = obj;
                    break;
                case TypeSM.Authorisation:
                    _authorisationExpects = obj;
                    break;
                case TypeSM.Message:
                    _messageExpects = obj;
                    break;
                case TypeSM.Error:
                    _errorExpects = obj;
                    break;
                case TypeSM.ChangingUserState:
                    _changingUserStateExpects = obj;
                    break;
                default:
                    throw new LocationServiceException(StringResources.Message_Location_Service_Exception);
            }
        }

        public void TransmitData(TypeSM type, object data)
        {
            switch (type)
            {
                case TypeSM.Registration:
                    {
                        if (_registrationExpects != null)
                            (_registrationExpects as ISubscriber).Update(data);
                        break;
                    }
                case TypeSM.Authorisation:
                    {
                        if (_authorisationExpects != null)
                            (_authorisationExpects as ISubscriber).Update(data);
                        break;
                    }
                case TypeSM.Message:
                    {
                        if (_messageExpects != null)
                            (_messageExpects as ISubscriber).Update(data);
                        break;
                    }
                case TypeSM.Error:
                    {
                        if (_errorExpects != null)
                            (_errorExpects as ISubscriber).Update(data);
                        break;
                    }
                case TypeSM.ChangingUserState:
                    {
                        if (_changingUserStateExpects != null)
                            (_changingUserStateExpects as ISubscriber).Update(data);
                        break;
                    }
                default:
                    throw new LocationServiceException(StringResources.Message_Location_Service_Exception);
            }
        }
        #endregion
    }
}
