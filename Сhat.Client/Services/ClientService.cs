﻿using Chat.Base.AbstractClasses;
using Chat.Base.Enums;
using Сhat.Base.Classes;
using Сhat.Client.Interfaces;
using Сhat.Client.View;
using Сhat.Client.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text.Json;
using System.Threading;
using Chat.Base.Interfaces;
using Chat.Base.View;
using Сhat.Client.Resources;
using Сhat.Client.Enums;

namespace Сhat.Client.Services
{
    public class ClientService: ClientBase
    {
        #region Constructors
        public ClientService(SourceServices sS, SynchronizationContext sC, string host, int port)
        {
            _sourceServices = sS;
            _synchronizationContext = sC;
            var client = new TcpClient(host, port);
            var steam = client.GetStream();
            User = new User();
            _reader = new StreamReader(steam);
            _writer = new StreamWriter(steam);
            _client = client;
            _locationServiceAlert = _sourceServices.GetLSA();
            _typeDiconect = TypeDiconect.BrokenConnection;
            Thread receiveThread = new Thread(new ThreadStart(ReceiveMessage));
            receiveThread.Start();
        }
        #endregion

        #region Fields
        
        private TcpClient _client;
        private StreamWriter _writer;
        private StreamReader _reader;
        private ILocationServiceAlert<ISubscriber> _locationServiceAlert;
        private SourceServices _sourceServices;
        private SynchronizationContext _synchronizationContext;
        private TypeDiconect _typeDiconect;
        #endregion

        #region Properties
        public User User { get; set; }
        #endregion

        #region Methods 
        public void ClientAuthorisation(string name)
        {
            User.Name = name;
            var mess = JsonSerializer.Serialize(User);
            SendMessages(TypeSM.Authorisation, mess);
        }

        private void ClientAuthorisation(User user)
        {
            if (User.Name == user.Name)
            {
                User = user;
                ClientAuthorisation(User.Name);
            }
        }

        public void ClientRegistration(string name)
        {
            User.Name = name;
            var mess = JsonSerializer.Serialize(User);
            SendMessages(TypeSM.Registration, mess);
        }

        public void GetMessages(User user)
        {
            var strMess = JsonSerializer.Serialize(user);
            SendMessages(TypeSM.Message, strMess);
        }

        public void SendMessages(User user, string message)
        {
            var mess = new Message
            {
                Contents = message,
                Recipient = user,
                Sender = User,
                Time = DateTime.Now.Ticks
            };
            var strMess = JsonSerializer.Serialize(mess);

            SendMessages(TypeSM.Message, strMess);
        }

        private void SendMessages(TypeSM typeSM, string date)
        {
            var mess = JsonSerializer.Serialize(new ServiceMessage()
            {
                Type = typeSM,
                Data = date,
            });
            try
            {
                _writer.WriteLine(mess);
                _writer.Flush();
            }
            catch { Disconnect(TypeDiconect.BrokenConnection); }
        }

        private void ReceiveMessage()
        {
            while (_client.Connected)
            {
                try
                {
                    var message = _reader.ReadLine();
                    ServiceMessage sMessage = JsonSerializer.Deserialize<ServiceMessage>(message);
                    var method = GetHandler(sMessage.Type);
                    method.Method.Invoke(this, new object[] { sMessage.Data });
                }
                catch
                {
                    switch (_typeDiconect)
                    {
                        case TypeDiconect.AuthorizationError:
                            {
                                Disconnect();
                                break;
                            }
                        case TypeDiconect.BrokenConnection:
                            {
                                Disconnect();
                                var action = new Action(() => {
                                    _locationServiceAlert.SetRegion(null, Regions.Lockout);
                                    _locationServiceAlert.SetRegion(null, Regions.Central);
                                    _locationServiceAlert.SetRegion(null, Regions.Left);
                                    _synchronizationContext.Send(_ =>
                                    {
                                        AuthorisationViewModel viewModel = new AuthorisationViewModel(_sourceServices);
                                        _locationServiceAlert.SetRegion(new AuthorisationView { DataContext = viewModel }, Regions.Central);
                                    }, null);
                                });

                                _synchronizationContext.Send(_ =>
                                {
                                    var view = new LockScreenView(StringResources.Message_Breaking_Сonnection, "Выход", action);
                                    _locationServiceAlert.SetRegion(view, Regions.Lockout);
                                }, null);
                                break;
                            }
                        case TypeDiconect.ManualDisconnect:
                            {
                                _locationServiceAlert.SetRegion(null, Regions.Central);
                                _locationServiceAlert.SetRegion(null, Regions.Left);

                                _synchronizationContext.Send(_ =>
                                {
                                    AuthorisationViewModel viewModel = new AuthorisationViewModel(_sourceServices);
                                    _locationServiceAlert.SetRegion(new AuthorisationView { DataContext = viewModel }, Regions.Central);
                                }, null);
                                break;
                            }
                    }
                }
            }
        }
        #endregion

        #region Disconnect
        public void Disconnect(TypeDiconect type)
        {
            _typeDiconect = type;
            Disconnect();
        }

        public void Disconnect()
        {
            if (_reader != null)
                _reader.Close();
            if (_writer != null)
                _writer.Close();
            if (_client != null)
                _client.Close();
        }
        #endregion

        #region Message Handler
        protected override void AuthorizationHandling(string date)
        {
            List<User> users = JsonSerializer.Deserialize<List<User>>(date);
            User = users.First(_ => _.Name == User.Name);
            _locationServiceAlert.TransmitData(TypeSM.Authorisation, users);
        }

        protected override void ErrorHandling(string date)
        {
            _locationServiceAlert.TransmitData(TypeSM.Error, date);
        }

        protected override void MessageHandling(string date)
        {
            var mess = JsonSerializer.Deserialize<List<Message>>(date);
            _locationServiceAlert.TransmitData(TypeSM.Message, mess);
        }

        protected override void RegistrationHandling(string date)
        {
            User user = JsonSerializer.Deserialize<User>(date);
            ClientAuthorisation(user);
        }

        protected override void ChangingUserStateHandling(string data)
        {
            User user = JsonSerializer.Deserialize<User>(data);
            _locationServiceAlert.TransmitData(TypeSM.ChangingUserState, user);
        }
        #endregion 
    }
}
