﻿using Chat.Base.Interfaces;
using System.Windows.Controls;
using Сhat.Client.Interfaces;

namespace Сhat.Client.View
{
    public partial class ChatView : UserControl, ISubscriber
    {
        #region Constructors
        public ChatView()
        {
            InitializeComponent();
        }
        #endregion

        #region Implementing ISubscriber
        public void Update(object data)
        {
            (DataContext as ISubscriber).Update(data);
        }
        #endregion
    }
}
