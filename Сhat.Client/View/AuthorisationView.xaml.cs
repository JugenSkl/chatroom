﻿using Chat.Base.Interfaces;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using Сhat.Client.Interfaces;

namespace Сhat.Client.View
{
    public partial class AuthorisationView : UserControl, ISubscriber
    {
        #region Constructors
        public AuthorisationView()
        {
            Application.Current.Startup += Current_Startup;
            InitializeComponent();
        }
        #endregion

        #region Fields
        private SynchronizationContext _synchronizationContext;
        #endregion

        #region Event Handling
        private void Current_Startup(object sender, StartupEventArgs e)
        {
            _synchronizationContext = SynchronizationContext.Current;
        }
        #endregion

        #region Implementing ISubscriber
        public void Update(object data)
        {
            _synchronizationContext.Send(_ =>
            {
                (DataContext as ISubscriber).Update(data);
            }, null);
        }
        #endregion
    }
}
