﻿using Chat.Base.Interfaces;
using System.Windows.Controls;
using Сhat.Client.Interfaces;
using Сhat.Client.ViewModel;

namespace Сhat.Client.View
{
    public partial class ListUsersView : UserControl, ISubscriber
    {
        #region Constructors
        public ListUsersView(ListUsersViewModel viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;
        }
        #endregion

        #region Implementing ISubscriber
        public void Update(object data)
        {
            (DataContext as ISubscriber).Update(data);
        }
        #endregion
    }
}
