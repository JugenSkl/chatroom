﻿using Chat.Base.Interfaces;
using System.Threading;
using System.Windows.Controls;
using Сhat.Client.Interfaces;

namespace Сhat.Client.View
{
    public partial class CreateAccountView : UserControl, ISubscriber
    {
        #region Constructors
        public CreateAccountView()
        {
            InitializeComponent();
            _synchronizationContext = SynchronizationContext.Current;
        }
        #endregion

        #region Fields
        private SynchronizationContext _synchronizationContext;
        #endregion

        #region Implementing ISubscriber
        public void Update(object data)
        {
            _synchronizationContext.Send(_ =>
            {
                (DataContext as ISubscriber).Update(data);
            }, null);
        }
        #endregion
    }
}
