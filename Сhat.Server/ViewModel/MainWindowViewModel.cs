﻿using Chat.Base.Interfaces;
using Chat.Base.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using Сhat.Base;
using Сhat.Server.Services;

namespace Chat.Server.ViewModel
{
    public class MainWindowViewModel: ViewModelBase
    {
        #region Constructors
        public MainWindowViewModel(ILocationService<UserControl> lS)
        {
            LocationS = lS;
        }
        #endregion

        #region Properties
        private ILocationService<UserControl> _locationS;
        public ILocationService<UserControl> LocationS
        {
            get { return _locationS; }
            set { SetProperty(ref _locationS, value); }
        }
        #endregion
    }
}
