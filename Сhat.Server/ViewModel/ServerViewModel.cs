﻿using Chat.Base.Enums;
using Chat.Base.Interfaces;
using Chat.Server.Database;
using Chat.Server.View;
using Chat.Server.ViewModel;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Сhat.Base;
using Сhat.Base.Classes;
using Сhat.Server.Interfaces;
using Сhat.Server.Services;
using Сhat.Server.TCP;

namespace Сhat.Server.ViewModel
{
    public class ServerViewModel: ViewModelBase, IRegistrarListener
    {
        #region Constructors
        public ServerViewModel(TCP.Server ser, ILocationService<UserControl> lS, IRegistrar reg)
        {
            _registrar = reg != null ? reg : Registrar.GetInstance();
            _registrar.Subscribe(this);
            _synchronizationContext = SynchronizationContext.Current;
            _locationService = lS;
            Clients = new ObservableCollection<User>(ManagerDatabase.GetInstance().GetUsers());
            Messages = new ObservableCollection<string>();
            ExitCommand = new DelegateCommand(DoExit);

            try
            {
                _server = ser;
                new Thread(new ThreadStart(_server.Listen)).Start();
                Application.Current.Exit += AplicationExit;
            }
            catch { _server.Disconnect(); }
        }
        #endregion

        #region Fields
        private SynchronizationContext _synchronizationContext;
        private ILocationService<UserControl> _locationService;
        private IRegistrar _registrar;
        private TCP.Server _server;
        #endregion

        #region Properties
        private ObservableCollection<User> _clients;
        public ObservableCollection<User> Clients
        {
            get { return _clients; }
            set { SetProperty(ref _clients, value); }
        }

        private ObservableCollection<string> _messages;
        public ObservableCollection<string> Messages
        {
            get { return _messages; }
            set { SetProperty(ref _messages, value); }
        }
        #endregion

        #region Methods
        private void AplicationExit(object sender, ExitEventArgs e)
        {
            _server.Disconnect();
        }
        #endregion

        #region Commands
        public DelegateCommand ExitCommand { get; private set; }
        private void DoExit()
        {
            _server.Disconnect();
        }
        #endregion

        #region Implementing IRegistrarListener
        public void Updata(string message)
        {
            if (Messages != null)
            {
                _synchronizationContext.Send(_ =>
                {
                    Messages.Add(message);
                }, null);

            }
        }

        public void Updata(User client)
        {
            if (client != null)
            {
                _synchronizationContext.Send(_ =>
                {
                    var user = Clients.FirstOrDefault(_ => _.Id == client.Id);
                    if (user != null)
                    {
                        user.IsOnline = client.IsOnline;
                    }
                    else
                    {
                        Clients.Add(client);
                    }
                }, null);

            }
        }
        #endregion
    }
}
