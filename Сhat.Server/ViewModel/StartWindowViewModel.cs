﻿using Chat.Base.Enums;
using Chat.Base.Interfaces;
using Chat.Base.View;
using Chat.Server.Resources;
using Chat.Server.View;
using Prism.Commands;
using System;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using Сhat.Base;
using Сhat.Server.Services;
using Сhat.Server.ViewModel;

namespace Chat.Server.ViewModel
{
    public class StartWindowViewModel: ViewModelBase
    {
        #region Constructors
        public StartWindowViewModel(ILocationService<UserControl> lS, string error = "")
        {
            _locationService = lS;
            Host = "127.0.0.1";
            Port = "8888";
            LaunchСommand = new DelegateCommand<string>(DoLaunch);
            ErroString = error;
        }
        #endregion

        #region Properties
        private string _host;
        public string Host
        {
            get { return _host; }
            set { SetProperty(ref _host, value); }
        }

        private string _port;
        public string Port
        {
            get { return _port; }
            set { SetProperty(ref _port, value); }
        }

        private string _erroString;
        public string ErroString
        {
            get { return _erroString; }
            set { SetProperty(ref _erroString, value); }
        }
        #endregion

        #region Fields
        private ILocationService<UserControl> _locationService;
        #endregion

        #region Commands
        public DelegateCommand<string> LaunchСommand { get; set; }
        private void DoLaunch(string port)
        {
            var view = new LockScreenView(StringResources.Message_Boot);
            _locationService.SetRegion(view, Regions.Lockout);

            try
            {
                int portInt = int.Parse(port);
                if (portInt >= 0 && portInt <= 65535)
                {
                    var reg = Registrar.GetInstance();
                    var server = new Сhat.Server.TCP.Server(reg, _locationService, Host, int.Parse(Port));
                    var viewModel = new ServerViewModel(server, _locationService, reg);
                    _locationService.SetRegion(new ServerView { DataContext = viewModel }, Regions.Central);
                    reg.AddMessage(string.Format(StringResources.Message_Server_Initialization, Host, Port));

                }
                else
                {
                    ErroString = StringResources.Message_Invalid_Port_Value;
                }
            }
            catch(FormatException e)
            {
                ErroString = StringResources.Message_Error_Port_Set_String;
            }
            catch (Exception e)
            {
                ErroString = e.Message;
            }
            finally
            {
                Task.Run(() =>
                {
                    Thread.Sleep(250);
                    _locationService.SetRegion(null, Regions.Lockout);
                });
            }
        }
        #endregion
    }
}
