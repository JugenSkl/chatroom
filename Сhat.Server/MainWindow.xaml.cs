﻿using Chat.Base.Enums;
using Chat.Base.Services;
using Chat.Server.View;
using Chat.Server.ViewModel;
using System.Windows;
using System.Windows.Controls;
using Сhat.Server.Services;

namespace Сhat.Server
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Constructors
        public MainWindow()
        {
            InitializeComponent();
            Registrar.Initialising();
            _locationService = new LocationService<UserControl>();
            DataContext = new MainWindowViewModel(_locationService);
            this.Loaded += MainWindow_Loaded; ;
        }
        #endregion

        #region Fields
        private LocationService<UserControl> _locationService;
        #endregion

        #region Event handlers
        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            var viewModel = new StartWindowViewModel(_locationService);
            _locationService.SetRegion(new StartWindowView() { DataContext = viewModel }, Regions.Central);
        }
        #endregion
    }
}
