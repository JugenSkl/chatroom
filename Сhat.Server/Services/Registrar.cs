﻿using System.Collections.Generic;
using Сhat.Base.Classes;
using Сhat.Server.Interfaces;

namespace Сhat.Server.Services
{
    public class Registrar : IRegistrar
    {
        #region Constructors
        private Registrar()
        {
            _listeners = new List<IRegistrarListener>();
            _notifications = new Queue<string>();
        }
        #endregion

        #region Singleton
        private static IRegistrar _registrar;

        public static void Initialising()
        {
            _registrar = new Registrar();
        }

        public static IRegistrar GetInstance()
        {
            return _registrar;
        }
        #endregion

        #region Fields
        private Queue<string> _notifications;
        private List<IRegistrarListener> _listeners;
        #endregion

        #region Methods
        private void NotifyMessage()
        {
            while (_notifications.Count > 0)
            {
                string mes = _notifications.Dequeue();
                foreach (var listener in _listeners)
                {
                    listener.Updata(mes);
                }
            }
        }
        #endregion

        #region Implementation IRegistrar
        public void Subscribe(IRegistrarListener obj)
        {
            if (_listeners != null)
            {
                _listeners.Add(obj);
                NotifyMessage();
            }
        }

        public void Unsubscribe(IRegistrarListener obj)
        {
            if (_listeners != null)
            {
                _listeners.Remove(obj);
            }
        }

        public void AddMessage(string mess)
        {
            if (_notifications != null)
            {
                _notifications.Enqueue(mess);
                if (_listeners != null && _listeners.Count != 0)
                {
                    NotifyMessage();
                }
            }
        }

        public void AddUser(User client)
        {
            if (_listeners != null && _listeners.Count != 0)
            {
                foreach (var listener in _listeners)
                {
                    listener.Updata(client);
                }
            }
        }

        public void Clear()
        {
            _listeners = new List<IRegistrarListener>();
            _notifications = new Queue<string>();
        }
        #endregion
    }
}
