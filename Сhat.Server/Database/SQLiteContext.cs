﻿using Microsoft.EntityFrameworkCore;
using System;
using System.IO;
using Сhat.Base.Classes;

namespace Chat.Server.Database
{
    public class SQLiteContext : DbContext
    {
        #region Constructors
        public SQLiteContext()
        {
            Database.EnsureCreated();
        }
        #endregion

        #region Properties
        public DbSet<User> Users { get; set; }
        public DbSet<Message> Messages { get; set; }
        public static string BaseAddress { get; set; }
        #endregion

        #region Methods
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite($"Data Source={BaseAddress};");
        }
        #endregion

    }
}
