﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Сhat.Base.Classes;
using Сhat.Server.Interfaces;

namespace Chat.Server.Database
{
    public class ManagerDatabase
    {
        #region Constructors
        private ManagerDatabase(string host, int port)
        {
            var str = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            SQLiteContext.BaseAddress = Path.Combine(str, "Chat", "Server", host + "_" + port.ToString() + ".db");
        }
        #endregion

        #region Singleton
        private static ManagerDatabase _instance;

        public static ManagerDatabase Initialize(string host, int port)
        {
            return _instance = new ManagerDatabase(host, port);
        }

        public static ManagerDatabase GetInstance()
        {
            return _instance;
        }

        public static void Clear()
        {
            _instance = null;
        }
        #endregion

        #region Methods
        public bool RegisterUser(string name)
        {
            try
            {
                using (SQLiteContext db = new SQLiteContext())
                {
                    var _us = db.Users.FirstOrDefault(_ => _.Name == name);
                    if (_us == null)
                    {
                        User user = new User { Name = name };

                        db.Users.Add(user);
                        db.SaveChanges();
                        return true;
                    }
                    return false;
                }
            }
            catch { return false; }
        }

        public List<User> GetUsers()
        {
            try
            {
                using (SQLiteContext db = new SQLiteContext())
                {
                    return db.Users.ToList();
                }
            }
            catch { return null; }
        }

        public User GetUser(string name)
        {
            try
            {
                using (SQLiteContext db = new SQLiteContext())
                {
                    return db.Users.FirstOrDefault(_ => _.Name == name);
                }
            }
            catch { return null; }
        }

        public bool RegisterMessages(Message message)
        {
            try
            {
                using (SQLiteContext db = new SQLiteContext())
                {
                    message.Sender = db.Users.FirstOrDefault(_ => _.Id == message.Sender.Id);
                    message.Recipient = db.Users.FirstOrDefault(_ => _.Id == message.Recipient.Id);
                    db.Messages.Add(message);
                    db.SaveChanges();
                    return true;
                }
            }
            catch { return false; }
        }

        public Message GetMessage(long time)
        {
            try
            {
                using (SQLiteContext db = new SQLiteContext())
                {
                    return db.Messages.Include(s => s.Sender).Include(r => r.Recipient).FirstOrDefault(_ => _.Time == time);
                }
            }
            catch { return null; }
        }

        public List<Message> GetMessages(int id)
        {
            try
            {
                using (SQLiteContext db = new SQLiteContext())
                {
                    return db.Messages.Include(s => s.Sender).Include(r => r.Recipient).Where(_ => _.Recipient.Id == id || _.Sender.Id == id).ToList();
                }
            }
            catch { return null; }
        }

        public List<Message> GetMessages(int idS, int idR)
        {
            try
            {
                using (SQLiteContext db = new SQLiteContext())
                {
                    return db.Messages.Include(s => s.Sender).Include(r => r.Recipient)
                        .Where(_ => _.Recipient.Id == idS && _.Sender.Id == idR || _.Recipient.Id == idR && _.Sender.Id == idS).ToList();
                }
            }
            catch { return null; }
        }
        #endregion
    }
}
