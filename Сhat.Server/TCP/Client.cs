﻿using Chat.Base.AbstractClasses;
using Chat.Base.Enums;
using Chat.Server.Database;
using Chat.Server.Resources;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Сhat.Base.Classes;
using Сhat.Server.Interfaces;

namespace Сhat.Server.TCP
{
    public class Client: ClientBase
    {
        #region Constructors
        public Client(TcpClient tcpClient, Server server, IRegistrar registrar)
        {
            _client = tcpClient;
            _reader = new StreamReader(_client.GetStream());
            _writer = new StreamWriter(_client.GetStream());
            _server = server;
            _registrar = registrar;
            Id = Guid.NewGuid().ToString();
            server.AddConnection(this);
        }
        #endregion

        #region Properties
        public string Id { get; set; }
        public User User { get; set; }
        #endregion

        #region Fields
        private Server _server;
        private TcpClient _client;
        private StreamReader _reader;
        private StreamWriter _writer;
        private IRegistrar _registrar;
        #endregion

        #region Methods
        public void Process()
        {
            try
            {
                while (true)
                {
                    var message = GetMessage();
                    ServiceMessage sMessage = JsonSerializer.Deserialize<ServiceMessage>(message);
                    var method = GetHandler(sMessage.Type);
                    method.Method.Invoke(this, new object[] { sMessage.Data });
                }
            }
            catch
            {
                if (User != null)
                {
                    User.IsOnline = false;
                    _registrar.AddMessage(String.Format(StringResources.Message_User_Disconnection, User));
                    var mess = JsonSerializer.Serialize(new ServiceMessage()
                    {
                        Type = TypeSM.ChangingUserState,
                        Data = JsonSerializer.Serialize(this.User)
                    });
                    _server.BroadcastMessage(mess, this.User.Id, true);
                }
            }
            finally
            {
                _server.RemoveConnection(this.Id);
                Close();
            }
        }

        public void SendMessage(string mess)
        {
            _writer.WriteLine(mess);
            _writer.Flush();
        }

        private string GetMessage()
        {
            return _reader.ReadLine();
        }

        public void Close()
        {
            if (_client != null)
                _client.Close();
        }
        #endregion

        #region Message Handlers
        protected override void AuthorizationHandling(string data)
        {
            var user = JsonSerializer.Deserialize<User>(data);
            if (_server.UserIsAuthorized(user))
            {
                var strMess = string.Format(StringResources.Message_Reauthorization, user.Name);
                _registrar.AddMessage(strMess);
                var mess = JsonSerializer.Serialize(new ServiceMessage()
                {
                    Type = TypeSM.Error,
                    Data = strMess,
                });
                _server.BroadcastMessage(mess, Id);
                return;
            }

            User = ManagerDatabase.GetInstance().GetUser(user.Name);
            if (User != null)
            {
                User.IsOnline = true;
                _registrar.AddMessage(string.Format(StringResources.Message_User_Connection, User));
                _registrar.AddUser(User);
                var dateMess = JsonSerializer.Serialize(_server.GetUsers());
                var mess = JsonSerializer.Serialize(new ServiceMessage()
                {
                    Type = TypeSM.Authorisation,
                    Data = dateMess,
                });
                _server.BroadcastMessage(mess, this.User.Id);

                dateMess = JsonSerializer.Serialize(User);
                mess = JsonSerializer.Serialize(new ServiceMessage()
                {
                    Type = TypeSM.ChangingUserState,
                    Data = dateMess,
                });
                _server.BroadcastMessage(mess, User.Id, true);
            }
            else
            {
                var strMess = string.Format(StringResources.Message_Error_Connecting_User, user);
                _registrar.AddMessage(strMess);
                var mess = JsonSerializer.Serialize(new ServiceMessage()
                {
                    Type = TypeSM.Error,
                    Data = strMess,
                });
                _server.BroadcastMessage(mess,  Id);
            }

        }

        protected override void ErrorHandling(string date)
        {
            throw new NotImplementedException();
            //TODO: Обрабочик сообщений об ошибках не используется в серверном клиенте.
        }

        protected override void MessageHandling(string data)
        {
            var user = JsonSerializer.Deserialize<User>(data);
            var mess = JsonSerializer.Deserialize<Message>(data);
            if (mess.Contents != null)
            {
                if (mess == null)
                    return;

                var mD = ManagerDatabase.GetInstance();
                if (mD.RegisterMessages(mess))
                {
                    var sysMess = String.Format(StringResources.Message_Successful_Message_Registration, User, mess.Recipient);
                    _registrar.AddMessage(sysMess);

                    mess = mD.GetMessage(mess.Time);

                    var strList = JsonSerializer.Serialize(new List<Message> { mess });
                    var strMes = JsonSerializer.Serialize(new ServiceMessage { Type = TypeSM.Message, Data = strList });

                    _server.BroadcastMessage(strMes, mess.Sender.Id, mess.Recipient.Id);
                }
                else
                {
                    var sysMess = String.Format(StringResources.Message_Registration_Error_Message, User, mess.Recipient);
                    _registrar.AddMessage(sysMess);
                }
            }
            if (user != null)
            {
                if (user == null)
                    return;
                var mD = ManagerDatabase.GetInstance();
                var strList = JsonSerializer.Serialize(mD.GetMessages(User.Id, user.Id));
                var strMes = JsonSerializer.Serialize(new ServiceMessage { Type = TypeSM.Message, Data = strList });
                _server.BroadcastMessage(strMes, User.Id);

                var sysMess = String.Format(StringResources.Message_Successful_Sending_Messages, User, user);
                _registrar.AddMessage(sysMess);
            }
        }

        protected override void RegistrationHandling(string data)
        {
            var user = JsonSerializer.Deserialize<User>(data);
            var mD = ManagerDatabase.GetInstance();
            if (mD.RegisterUser(user.Name))
            {
                _registrar.AddMessage(string.Format(StringResources.Message_Successful_Registration, user.Name));
                var dateMess = JsonSerializer.Serialize(mD.GetUser(user.Name));

                var mess = JsonSerializer.Serialize(new ServiceMessage()
                {
                    Type = TypeSM.Registration,
                    Data = dateMess,
                });
                _server.BroadcastMessage(mess);
            }
            else
            {
                var strMess = string.Format(StringResources.Message_Registration_Error, user.Name);
                _registrar.AddMessage(strMess);
                var mess = JsonSerializer.Serialize(new ServiceMessage()
                {
                    Type = TypeSM.Error,
                    Data = strMess,
                });
                _server.BroadcastMessage(mess);
            }
        }

        protected override void ChangingUserStateHandling(string data)
        {
            throw new NotImplementedException();
            //TODO: Обрабочик подключений пользователя не используется в серверном клиенте.
        }
        #endregion
    }
}
