﻿using Chat.Base.Enums;
using Chat.Base.Interfaces;
using Chat.Server.Database;
using Chat.Server.Resources;
using Chat.Server.View;
using Chat.Server.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using Сhat.Base.Classes;
using Сhat.Server.Interfaces;
using Сhat.Server.Services;

namespace Сhat.Server.TCP
{
    public class Server
    {
        #region Constructors
        public Server(string address = "127.0.0.1", int port = 8888) : this(null, null, address, port) { }
        public Server(IRegistrar reg, ILocationService<UserControl> lS, string address = "127.0.0.1", int port = 8888)
        {
            _synchronizationContext = SynchronizationContext.Current;
            _registrar = reg != null ? reg : Registrar.GetInstance();
            _clients = new List<Client>();
            _locationService = lS;
            _address = IPAddress.Parse(address);
            _port = port;
            _isExit = false;
            ManagerDatabase.Initialize(address, port);
        }
        #endregion

        #region Fields
        private bool _isExit;
        private int _port;
        private IPAddress _address;
        private TcpListener _listener;
        private List<Client> _clients;
        private ILocationService<UserControl> _locationService;
        private SynchronizationContext _synchronizationContext;
        private IRegistrar _registrar;
        #endregion

        #region Methods
        public void AddConnection(Client client)
        {
            _clients.Add(client);
        }

        public void RemoveConnection(string id)
        {
            Client client = _clients.FirstOrDefault(c => c.Id == id);
            if (client != null)
                _clients.Remove(client);
            if (client.User != null)
            {
                _registrar.AddMessage(string.Format(StringResources.Message_User_Disconnection_Server, client.User));
                client.User.IsOnline = false;
                _registrar.AddUser(client.User);
            }
        }

        internal void Listen()
        {
            try
            {
                _listener = new TcpListener(_address, _port);
                _listener.Start();
                _registrar.AddMessage(string.Format(StringResources.Message_Waiting));
                while (true)
                {
                    TcpClient tcpClient = _listener.AcceptTcpClient();
                    Client client = new Client(tcpClient, this, _registrar);
                    Thread clientThread = new Thread(new ThreadStart(client.Process));
                    clientThread.Start();
                }
            }
            catch (SocketException e)
            {
                if (!_isExit)
                {
                    _registrar.AddMessage(string.Format(StringResources.Message_Server_Error));
                    Disconnect(string.Format(StringResources.Message_Port_Usage, _port));
                }
            }
        }

        protected internal void BroadcastMessage(string message)
        {
            foreach (var client in _clients)
                client.SendMessage(message);
        }

        protected internal void BroadcastMessage(string message, int id, bool isExclude = false)
        {
            if (isExclude)
            {
                foreach (var client_1 in _clients)
                {
                    if (client_1.User.Id != id)
                    {
                        client_1.SendMessage(message);
                    }
                }
            }
            else
            {
                foreach (var client_2 in _clients)
                {
                    if (client_2.User.Id == id)
                    {
                        client_2.SendMessage(message);
                    }
                }
            }
        }

        protected internal void BroadcastMessage(string message, string id)
        {
            foreach (var client in _clients)
            {
                if (client.Id == id)
                {
                    client.SendMessage(message);
                }
            }
        }

        protected internal void BroadcastMessage(string message, int idSender, int idRecipient)
        {
            foreach (var client in _clients)
            {
                if (client.User.Id == idSender || client.User.Id == idRecipient)
                    client.SendMessage(message);
            }
        }

        protected internal void Disconnect(string error = "")
        {
            _isExit = true;
            _listener.Stop();
            for (int i = 0; i < _clients.Count; i++)
            {
                _clients[i].Close();
            }
            _synchronizationContext.Send(_ =>
            {
                var viewModel = new StartWindowViewModel(_locationService, error);
                _locationService.SetRegion(new StartWindowView() { DataContext = viewModel }, Regions.Central);
            }, null);
            _registrar.Clear();
            ManagerDatabase.Clear();
        }

        public List<User> GetUsers()
        {
            var mD = ManagerDatabase.GetInstance();
            var result = mD.GetUsers();
            foreach (var client in _clients)
            {
                var user = result.FirstOrDefault(_ => _.Name == client.User.Name);
                if (user != null)
                    user.IsOnline = true;
            }
            return result;
        }

        public bool UserIsAuthorized(User user)
        {
            if (user == null)
                return false;

            return _clients.FirstOrDefault(client =>
            {
               if (client.User == null)
                   return false;
               return client.User.Name == user.Name;
            }) != null;
        }
        #endregion
    }
}
