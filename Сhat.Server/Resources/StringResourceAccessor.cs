﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chat.Server.Resources
{
    public class StringResourceAccessor
    {
        private static StringResources _resources = new StringResources();

        public static StringResources StringResources { get { return _resources; } }
    }
}