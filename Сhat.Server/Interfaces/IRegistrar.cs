﻿using Сhat.Base.Classes;

namespace Сhat.Server.Interfaces
{
    public interface IRegistrar
    {
        public void Subscribe(IRegistrarListener obj);
        public void Unsubscribe(IRegistrarListener obj);
        public void AddMessage(string mess);
        public void AddUser(User user);
        public void Clear();
    }
}
