﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chat.Base.Enums
{
    public enum TypeSM
    {
        Registration,
        Authorisation,
        ChangingUserState,
        Message,
        Error
    }
}
