﻿using Chat.Base.Enums;
using Chat.Base.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using Сhat.Base;

namespace Chat.Base.Services
{
    public class LocationService<T> : ViewModelBase, ILocationService<T>
    {
        #region Constructors
        public LocationService() { }
        #endregion

        #region Properties
        protected T _centralRegion;
        public T CentralRegion
        {
            get { return _centralRegion; }
            protected set { SetProperty(ref _centralRegion, value); }
        }

        protected T _leftRegion;
        public T LeftRegion
        {
            get { return _leftRegion; }
            protected set { SetProperty(ref _leftRegion, value); }
        }

        protected T _rightRegion;
        public T RightRegion
        {
            get { return _rightRegion; }
            protected set { SetProperty(ref _rightRegion, value); }
        }

        protected T _topRegion;
        public T TopRegion
        {
            get { return _topRegion; }
            protected set { SetProperty(ref _topRegion, value); }
        }

        protected T _lockoutRegion;
        public T LockoutRegion
        {
            get { return _lockoutRegion; }
            protected set { SetProperty(ref _lockoutRegion, value); }
        }
        #endregion

        #region Methods
        public void SetRegion(T obj, Regions region)
        {
            switch (region)
            {
                case Regions.Central:
                    CentralRegion = obj;
                    break;
                case Regions.Left:
                    LeftRegion = obj;
                    break;
                case Regions.Right:
                    RightRegion = obj;
                    break;
                case Regions.Top:
                    TopRegion = obj;
                    break;
                case Regions.Lockout:
                    LockoutRegion = obj;
                    break;
                default:
                    throw new LocationServiceException("Transmitted an unknown region type");
            }
        }
        #endregion

        #region Exception
        protected class LocationServiceException : ArgumentException
        {
            public LocationServiceException(string message) : base(message) { }
        }
        #endregion
    }
}
