﻿using Chat.Base.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chat.Base.AbstractClasses
{
    public abstract class ClientBase
    {
        #region Constructors
        public ClientBase()
        {
            _handlerCollection = new Dictionary<TypeSM, Delegate>();
            _handlerCollection.Add(TypeSM.Authorisation, new Action<string>(AuthorizationHandling));
            _handlerCollection.Add(TypeSM.Message, new Action<string>(MessageHandling));
            _handlerCollection.Add(TypeSM.Error, new Action<string>(ErrorHandling));
            _handlerCollection.Add(TypeSM.Registration, new Action<string>(RegistrationHandling));
            _handlerCollection.Add(TypeSM.ChangingUserState, new Action<string>(ChangingUserStateHandling));
        }
        #endregion

        #region Fields
        private Dictionary<TypeSM, Delegate> _handlerCollection;
        #endregion

        #region Methods
        public Delegate GetHandler(TypeSM typeSM)
        {
            return _handlerCollection[typeSM];
        }

        protected abstract void AuthorizationHandling(string data);
        protected abstract void ErrorHandling(string data);
        protected abstract void MessageHandling(string data);
        protected abstract void RegistrationHandling(string data);
        protected abstract void ChangingUserStateHandling(string data);
        #endregion
    }
}
