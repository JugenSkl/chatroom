﻿using Chat.Base.Interfaces;
using Chat.Base.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Chat.Base.View
{
    public partial class LockScreenView : UserControl, ISubscriber
    {
        #region Constructors
        public LockScreenView(string message)
        {
            InitializeComponent();
            DataContext = new LockScreenViewModel(message);
        }

        public LockScreenView(string message, string nameCommand, Action action)
        {
            InitializeComponent();
            DataContext = new LockScreenViewModel(message, nameCommand, action);
        }
        #endregion

        #region Implementation ISubscriber
        public void Update(object data)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
