﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Сhat.Base;
using Prism.Commands;
using System.Windows;

namespace Chat.Base.ViewModel
{
    public class LockScreenViewModel: ViewModelBase
    {
        #region Constructors
        public LockScreenViewModel(string message)
        {
            Message = message;
            TypeForme = TypeForm.One;
        }

        public LockScreenViewModel(string message, string nameCommand, Action action)
        {
            Message = message;
            TypeForme = TypeForm.One;
            TypeForme = TypeForm.Multi;
            NameCommand = nameCommand;
            DoCommand = new DelegateCommand(action);
        }
        #endregion

        #region Properties
        private string _message;
        public string Message
        {
            get { return _message; }
            set { SetProperty(ref _message, value); }
        }

        private string _nameCommand;
        public string NameCommand
        {
            get { return _nameCommand; }
            set { SetProperty(ref _nameCommand, value); }
        }

        private TypeForm _typeForme;
        public TypeForm TypeForme
        {
            get { return _typeForme; }
            set { SetProperty(ref _typeForme, value); }
        }
        #endregion

        #region Commands
        public DelegateCommand DoCommand { get; set; }
        #endregion

        #region Enums
        public enum TypeForm
        {
            One,
            Multi
        }
        #endregion
    }
}
