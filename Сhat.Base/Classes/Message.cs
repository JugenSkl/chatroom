﻿using Chat.Base.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Сhat.Base.Classes
{
    public class Message
    {
        #region Properties
        public int Id { get; set; }
        public User Sender { get; set; }
        public User Recipient { get; set; }
        public string Contents { get; set; }
        public long Time { get; set; }

        [NotMapped]
        public HorizontalAlignment Orientation { get; set; }
        [NotMapped]
        public DateTime Date
        {
            get { return new DateTime(Time); }
        }
        #endregion
    }
}
