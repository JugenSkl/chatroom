﻿using Chat.Base.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Сhat.Base.Classes
{
    public class ServiceMessage
    {
        #region Properties
        public TypeSM Type { get; set; }
        public string Data { get; set; }
        #endregion
    }
}
