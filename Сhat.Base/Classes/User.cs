using System.ComponentModel.DataAnnotations.Schema;

namespace Сhat.Base.Classes
{
    public class User: ViewModelBase
    {
        #region Properties
        public int Id { get; set; }
        public string Name { get; set; }

        private bool _isOnline;
        [NotMapped]
        public bool IsOnline
        {
            get { return _isOnline; }
            set { SetProperty(ref _isOnline, value); }
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return string.Format("{0} ({1})", Name, Id);
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() != this.GetType()) return false;

            var user = obj as User;
            return this.Name == user.Name && this.Id == user.Id;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode() + Id.GetHashCode();
        }
        #endregion
    }
}
