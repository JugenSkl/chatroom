﻿using Chat.Base.Enums;

namespace Chat.Base.Interfaces
{
    public interface ILocationService<T>
    {
        #region Properties
        public T LockoutRegion { get; }
        public T CentralRegion { get; }
        public T LeftRegion { get; }
        public T RightRegion { get; }
        public T TopRegion { get; }
        #endregion

        #region Methods
        public void SetRegion(T obj, Regions region);
        #endregion
    }
}
