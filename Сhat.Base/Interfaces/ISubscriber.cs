﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chat.Base.Interfaces
{
    public interface ISubscriber
    {
        #region Methods
        public void Update(object data);
        #endregion
    }
}
